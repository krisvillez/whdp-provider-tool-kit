
###################
### ENVIRONMENT ###
###################

import csv
import codecs
import datetime
import ftplib
import os
import pandas
import platform
import shutil
import socket
import sys
import xlrd
import yaml
 

class whdpkit:
    """WHDP tool kit
    """
    
    def __init__(self):
        """Create an instance of whdpkit"""
        
        AtWHDP = (platform.node() == 'eng-whdp1') # "AtWHDP" means the script is used on the WHDP1 server - otherwise local test assumed
        self.AtWHDP = AtWHDP
        
        currdir=os.getcwd()
        if AtWHDP:
            path_dlz               = "/home/whdp-provider/dlz"
            path_data_target       = "/home/whdp-provider/dlz/data"
            path_manual            = "/home/whdp-provider/local_data/manual"
            path_manual_archive    = "/home/whdp-provider/local_data/manual/archive"
            path_lab_local_archive = "/home/whdp-provider/local_data/lab/archive"
            path_lab_local         = "/home/whdp-provider/local_data/lab"
            path_lab_source        = "/home/whdp-provider/dlz_maintenance/proxy/lab_source"
            path_lab_target        = "/home/whdp-provider/dlz/lab_data" # FOR SEMI-AUTOMATIC UPDATES OF persons.yaml IN DEVELOPMENT LANDING ZONE
            path_lab_operational   = "/data/landing_zone/lab_data" # FOR AUTOMATIC UPDATES OF lab_results.csv IN OPERATIONAL LANDING ZONE
            path_log               = "/home/whdp-provider/log"
            path_plcgrey_archive   = "/home/whdp-provider/local_data/plcgrey/archive"
            path_plcgrey_converted = "/home/whdp-provider/local_data/plcgrey"
            #path_plcgrey_landing    = "/home/whdp-provider/dlz/data/plcgrey/raw_data" # FOR TESTING IN DEVELOPMENT LANDING ZONE
            path_plcgrey_landing   = "/data/landing_zone/data/plcgrey/raw_data" # FOR USE IN OPERATIONAL LANDING ZONE
            filepass = '/home/whdp-provider/meta/pass.csv'
        else:
            path_dlz               = "../../dlz"
            path_data_target       = "../../dlz/data"
            path_manual            = "../../local_data/manual"
            path_manual_archive    = "../../local_data/manual/archive"
            path_lab_local_archive = "../../local_data/lab/archive"
            path_lab_local         = "../../local_data/lab"
            path_lab_source        = "../../Z_remote_data_lab"
            path_lab_target        = "../../dlz/lab_data"
            path_lab_operational   = "../../dlz/lab_data"
            path_log               = "log"
            path_plcgrey_archive   = "../../local_data/plcgrey/archive"
            path_plcgrey_converted = "../../local_data/plcgrey"
            path_plcgrey_landing   = "../../dlz/data/plcgrey/raw_data"
            
            path_log        = os.path.join(currdir,path_log)
            filepass = '../../meta/pass.csv'
              
        self.path_data_target        = path_data_target
        self.path_dlz = path_dlz
        self.path_manual = path_manual
        self.path_manual_archive = path_manual_archive
        self.path_lab_local_archive = path_lab_local_archive
        self.path_lab_local = path_lab_local
        self.path_lab_source = path_lab_source
        self.path_lab_target = path_lab_target
        self.path_lab_operational = path_lab_operational
        self.path_log        = path_log
        self.path_plcgrey_archive = path_plcgrey_archive
        self.path_plcgrey_converted = path_plcgrey_converted
        self.path_plcgrey_landing = path_plcgrey_landing
        
        self.whdpprovider = 'carina.doll@eawag.ch'
        
        dt  = datetime.datetime.now()
        dtstr0 = dt.strftime('%Y%m%d_%H%M%S')
        self.log_c_lab = os.path.join(path_log,'c_lab_'+platform.node()+'_'+dtstr0+'.csv')
        self.log_c_plcgrey = os.path.join(path_log,'c_plcgrey_'+platform.node()+'_'+dtstr0+'.csv')
        self.log_t_lab = os.path.join(path_log,'t_lab_'+platform.node()+'_'+dtstr0+'.csv')
        self.log_t_plcgrey = os.path.join(path_log,'t_plcgrey_'+platform.node()+'_'+dtstr0+'.csv')
        self.log_dlz_person = os.path.join(path_log,'dlz_person_'+platform.node()+'_'+dtstr0+'.csv')
        self.log_dlz_plcgrey = os.path.join(path_log,'dlz_plcgrey_'+platform.node()+'_'+dtstr0+'.csv')
        self.log_dlz_memographgrey = os.path.join(path_log,'dlz_memographgrey_'+platform.node()+'_'+dtstr0+'.csv')
        self.log_dlz_site = os.path.join(path_log,'dlz_site_'+platform.node()+'_'+dtstr0+'.csv')
        self.log_dlz_variable = os.path.join(path_log,'dlz_variable_'+platform.node()+'_'+dtstr0+'.csv')
        
        self.connect_lab = {'device': 'lab', 'host': '123.45.678.90', 'dir': r"\\\\R-lyeh\\city$\\data",'login': 'cthulhu', 'pw': "Ph`glui mglw`nafh Cthulhu R`lyeh wgah`nagl fhtagn"}
        self.connect_plcgrey = {'device': 'plcgrey', 'host': '123.45.678.90', 'dir': r"\\\\R-lyeh\\city$\\data",'login': 'cthulhu', 'pw': "Ph`glui mglw`nafh Cthulhu R`lyeh wgah`nagl fhtagn"}
                            
        
        print(filepass)
        if os.path.isfile(filepass):
            print("Found pass file - Updating passwords")
            with open(filepass) as csvfile:
                reader = csv.DictReader(csvfile, delimiter=';')
                for row in reader:
                    if row['device']=='plcgrey':
                        self.connect_plcgrey = row
                    elif row['device']=='lab':
                        self.connect_lab = row
        else:
            print("No pass file - Keeping default passwords")
            
        self.horizon = 360 # determines how old files can be to be eligible for FTP transfer [days]
        # self.horizon_source must be higher than self.horizon_target. Otherwise, the same file will transferred multiple times to the WHDP.
        self.horizon_source = 32 # determines how old files must be before they are removed at the source [days]
        self.horizon_target = 42 # determines how old files must be before they are removed at the local copy [days]
        return
    
    def addfolder(self,path):
        if (not os.path.isdir(path) ):
            # mkdir
            os.mkdir( path, 0o775 )
        return

    def dlzinit(self,csvfile):
        """ Initialize DLZ """
        
        self.logging(csvfile,'* Initiate DLZ ..')
        
        path_dlz = self.path_dlz
        path_data = os.path.join(path_dlz,'data')
        path_lab_data = os.path.join(path_dlz,'lab_data')
        path_sites = os.path.join(path_dlz,'sites')
        
        if (not os.path.isdir(path_dlz) ):
            if self.AtWHDP:
                # pool start-develop dlz
                command = 'pool start-develop dlz'
                os.system(command)
            else:
                # mkdir
                os.mkdir( path_dlz,755 );

        if (not os.path.isdir(path_data) ):
            if self.AtWHDP:
                self.logging(csvfile,'ERROR - folder should already exist: '+path_data)
                
                subject = '[WHDPauto-ERROR] folder should already exist: '+path_data
                body = 'ERROR whdpkit.py line 118.'
                self.sendmail(csvfile,subject,body)
                        
            else:
                # mkdir
                os.mkdir( path_data,755 );
        
        if (not os.path.isdir(path_lab_data) ):
            if self.AtWHDP:
                self.logging(csvfile,'ERROR - folder should already exist: '+path_lab_data)
                
                subject = '[WHDPauto-ERROR] folder should already exist: '+path_lab_data
                body = 'ERROR whdpkit.py line 130.'
                self.sendmail(csvfile,subject,body)
            else:
                # mkdir
                os.mkdir( path_lab_data,755 );
        
        if (not os.path.isdir(path_sites) ):
            if self.AtWHDP:
                self.logging(csvfile,'ERROR - folder should already exist: '+path_sites)
                
                subject = '[WHDPauto-ERROR] folder should already exist: '+path_sites
                body = 'ERROR whdpkit.py line 140.'
                self.sendmail(csvfile,subject,body)
            else:
                # mkdir
                os.mkdir( path_sites,755 );
            
        self.logging(csvfile,'* Initiate DLZ done')
        self.path_dlz = path_dlz 
        self.path_data = path_data 
        self.path_sites = path_sites 
        self.path_lab_data = path_lab_data 
        return
    
    def logging(self,csvfile,message):
        """ Command line message / Log file writing """
        print(message)
        with codecs.open(csvfile, "a", "utf-8") as file:
            file.write(message + '\n')
            
        return
    
    def removefile(self,csvfile,filepath):
        """ Remove file if it exists """
        FileExists = os.path.isfile(filepath)
        if FileExists:
            self.logging(csvfile,'*** Deleting: '+filepath+' ..')
            os.remove(filepath)
            self.logging(csvfile,'*** Deleting: '+filepath+' done')
            
        return
    
    def sendmail(self,csvfile,subject,body):
        """ Send an email to whdpprovider """
        
        if self.AtWHDP:
            message = '* Sending email ..'
            self.logging(csvfile,message)
            command = 'echo "'+body+'" | mail -s "'+subject+'" '+self.whdpprovider
            os.system(command)
            message = '* Sending email done'
            self.logging(csvfile,message)
            
        return
    
    def setupftp(self,device,csvfile):
        """ Command line message / Log file writing """
        
        connected = False
        f = []
        try:
            message = str ('** Connecting to host "%s"' % device['host'])
            self.logging(csvfile, message)
            f = ftplib.FTP(device['host'])
            message = str ('** Connected to host "%s"' % device['host'])
            self.logging(csvfile, message)
            
            try:
                f.login(device['login'], device['pw'])
                self.logging(csvfile,str ('** Logged in successfully'))
                connected = True
                self.logging(csvfile,str ('* FTP connection attempt successful'  ))
            except ftplib.error_perm:
                self.logging(csvfile,str ('ERROR: cannot login'))
                f.quit
                
                subject = '[WHDPauto-ERROR] cannot login'
                body = 'ERROR whdpkit.py line 204.'
                self.sendmail(log,subject,body)
                
        except (socket.error or socket.gaierror):
            self.logging(csvfile,str ('ERROR: cannot reach "%s"' % device['host']))

            subject = str ('[WHDPauto-ERROR] cannot reach "%s"' % device['host'])
            body = 'ERROR whdpkit.py line 211.'
            self.sendmail(csvfile,subject,body)
    
        return connected, f