
###################
### ENVIRONMENT ###
###################

import datetime
import os
import shutil
import sys

sys.path.append('../..')
print(os.getcwd())

from whdpprovider.whdpkit import whdpkit

whdpinst = whdpkit()

###############
###  START  ###
###############
log = whdpinst.log_t_plcgrey
whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

########################################################################
### SETUP FTP CONNECTION                                             ### 
########################################################################

connected , f = whdpinst.setupftp(whdpinst.connect_plcgrey,log)

    
########################################################################
### FILE TRANSFER                                           ### 
########################################################################

if connected:
    message = '* attempting change of working directory on PLC to: '+whdpinst.connect_plcgrey['dir']
    whdpinst.logging(log,message)
    
    f.cwd(whdpinst.connect_plcgrey['dir'])
    
    message = '* successful change of working directory on PLC to: '+whdpinst.connect_plcgrey['dir']
    whdpinst.logging(log,message)
    
    # Current timestamp
    t_now = datetime.datetime.now().date()
    currdir=os.getcwd() ####
    
    whdpinst.logging(log,\
            '* Starting file transfer')
        
    filematch = 'logfileGr*.csv'
        
    # List files in archive:
    files_archived = os.listdir(whdpinst.path_plcgrey_archive)
    fl = f.nlst(filematch)
        
    whdpinst.logging(log,\
            '*** Number of files: '+str(len(fl)))
    
    for filename in files_archived:
        if filename in fl:
            pass
        else:
            os.remove(os.path.join(whdpinst.path_plcgrey_archive, filename))
            whdpinst.logging(log,\
                             '*** '+filename+': removed from archive')
        pass
    
    s = 0;
    for filename in fl:
        
        t_file = datetime.datetime.strptime(filename[11:21], "%Y_%m_%d").date()
        delta = (t_now-t_file).days
        if (delta<=whdpinst.horizon) and (delta>=1):

            if os.path.isfile(os.path.join(whdpinst.path_plcgrey_archive, filename)):
                whdpinst.logging(log,\
                             '*** '+filename+': exists already')
                if whdpinst.AtWHDP and delta>whdpinst.horizon_source:
                    # File at source has been copied AND is older than pre-determined time window -> remove file at source
                    f.delete(filename)
                    whdpinst.logging(log,\
                                     '*** '+filename+': removed from source')
            else:
                
                whdpinst.logging(log,\
                             '*** '+filename+': collecting file .. ')
                   
                # COPY FILE TO LANDING ZONE
                with open(filename, 'wb') as fhandle:
                    f.retrbinary('RETR ' + filename, fhandle.write)
                    pass
                
                # COPY FILE TO ARCHIVE
                shutil.copy2(filename,os.path.join(whdpinst.path_plcgrey_archive,filename) )
                
                # RENAME FILE IN LANDING ZONE
                newfilename = 'data-'+filename+'.raw' 
                os.rename(filename, newfilename)
                
                # COPY FILE TO LANDING ZONE
                shutil.copy2(newfilename,os.path.join(whdpinst.path_plcgrey_landing,newfilename) )
                
                # REMOVE FILES
                os.remove(newfilename )
                whdpinst.logging(log,\
                             '*** '+filename+': collecting file done')
                
                s = s + 1
        elif (delta>whdpinst.horizon):
            whdpinst.logging(log,\
                             '*** '+filename\
                             +': older than '+str(t_horizon)+' days - removal [Not executed]')
            
    whdpinst.logging(log,'*** Number of downloads: '+str(s))
    whdpinst.logging(log,'* Closing FTP connection ')
    
    f.quit()
    
###############
###  EMAIL  ###
###############

subject = '[WHDPauto] Copied plcgrey files succesfully.'
body = 'Files were transfered succesfully from plcgrey via FTP.'
whdpinst.sendmail(log,subject,body)

############
### DONE ###
############

whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
whdpinst.logging(log,'* DONE ')