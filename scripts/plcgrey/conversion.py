# Python conversion script for PLC grey, v0.1
# Original: Feb 02, 2019 -- Kris Villez
# Last mod: Feb 02, 2019 -- Kris Villez

# ---> 1.) load required packages (optional)
import os
import pandas

def convert(raw_file, output_file):
    
    print('==============================================================')
    
    # ---> 2.) read file

    if not os.path.isfile(raw_file):
        raise ValueError('Error: Input File does not exist.')
        
    print('* Reading file: '+raw_file)
    raw_data = pandas.read_csv(raw_file,delimiter  =';',header =0, encoding="latin-1")
    print('* Reading file done ')
    
    
    # ---> 3.) test properties

    header = raw_data.columns.values
    
    if not('Date' in header):
        raise ValueError('ERROR: Could not find column named "Date" in '+raw_file+'.')
    
    if not('Time' in header):
        raise ValueError('ERROR: Could not find column named "Time" in '+raw_file+'.')
    
    nRow =raw_data.shape[0]
    nCol = raw_data.shape[1]

    # ---> 4.) add additional information (optional)
    
    source_type = "plc_grey"
    # To update the following dictionary: use PLCconfig2list.ipynb
    sensors = \
[{'source_type': 'plcgrey', 'plcprog': 'prgB102', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M1', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB103', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M1', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB104', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M1', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB105', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M1', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB111', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M1', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB112', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M1', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB102', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M1', 'varname': 'bt'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB103', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M1', 'varname': 'bq_phvolt'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB104', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M1', 'varname': 'bq_ph'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB105', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M1', 'varname': 'bq_cond'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB111', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M1', 'varname': 'bq_dopp'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB112', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M1', 'varname': 'bq_doconc'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB202', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M2', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB203', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M2', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB204', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M2', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB205', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M2', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB211', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M2', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB212', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M2', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgM201', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M2', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB202', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M2', 'varname': 'bt'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB203', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M2', 'varname': 'bq_phvolt'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB204', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M2', 'varname': 'bq_ph'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB205', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M2', 'varname': 'bq_cond'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB211', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M2', 'varname': 'bq_dopp'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB212', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M2', 'varname': 'bq_doconc'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB302', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M3', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB303', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M3', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB304', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M3', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB311', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M3', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB312', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M3', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB320', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M3', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgM301', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M3', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB302', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M3', 'varname': 'bt'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB303', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M3', 'varname': 'bq_phvolt'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB304', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M3', 'varname': 'bq_ph'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB311', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M3', 'varname': 'bq_dopp'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB312', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M3', 'varname': 'bq_doconc'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB320', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M3', 'varname': 'bq_turb'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB402', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M4', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB403', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M4', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB404', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M4', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB411', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M4', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB412', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M4', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgM401', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_M4', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB402', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M4', 'varname': 'bt'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB403', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M4', 'varname': 'bq_phvolt'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB404', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M4', 'varname': 'bq_ph'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB411', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M4', 'varname': 'bq_dopp'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB412', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_M4', 'varname': 'bq_doconc'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB515', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_MBRTank1', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB516', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_MBRTank2', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgY501', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_MBRTank1', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB515', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_MBRTank1', 'varname': 'bp'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB516', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_MBRTank2', 'varname': 'bp'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB615', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_GAC1-W0', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB616', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_GAC1-W4', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgY601', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_GAC1-Pipe', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB615', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_GAC1-W0', 'varname': 'bp'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB616', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_GAC1-W4', 'varname': 'bp'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB701', 'plcvar': 'iStatus', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB702', 'plcvar': 'iStatus', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB703', 'plcvar': 'iStatus', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB704', 'plcvar': 'iStatus', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB705', 'plcvar': 'iStatus', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB706', 'plcvar': 'iStatus', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB707', 'plcvar': 'iStatus', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB708', 'plcvar': 'iStatus', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB715', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_GAC2-BufferTank', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgM701', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_GAC2-BufferTank', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgM702', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_GAC2-ColumnA', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgM703', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_GAC2-ColumnB', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgM704', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_GAC2-ColumnC', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgM705', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_GAC2-ColumnD', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgM706', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_GAC2-ColumnB', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgY701', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_GAC2-BufferTank', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgY702', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_GAC2-ColumnB', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgY703', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_GAC2-ColumnD', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB701', 'plcvar': 'rValueOut', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB702', 'plcvar': 'rValueOut', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB703', 'plcvar': 'rValueOut', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB704', 'plcvar': 'rValueOut', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB705', 'plcvar': 'rValueOut', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB706', 'plcvar': 'rValueOut', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB707', 'plcvar': 'rValueOut', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB708', 'plcvar': 'rValueOut', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB715', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_GAC2-BufferTank', 'varname': 'bp'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB815', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_Storage1', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB816', 'plcvar': 'iStatus', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgM801', 'plcvar': 'iStatus', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgM802', 'plcvar': 'iStatus', 'site': 'undefined', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgY801', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_Storage1', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgY802', 'plcvar': 'iStatus', 'site': 'NE_WaterHub_GW_GW_Storage2', 'varname': 'status'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB815', 'plcvar': 'rValueOut', 'site': 'NE_WaterHub_GW_GW_Storage1', 'varname': 'bp'}, 
 {'source_type': 'plcgrey', 'plcprog': 'prgB816', 'plcvar': 'iStatus', 'site': 'undefined', 'varname': 'status'}]
    
    df_sensors = pandas.DataFrame(sensors)
    
    # ---> 5.) reformat data

    #data = pandas.DataFrame(columns=['timestamp','value','site','source','variable'])
    dictData = {'timestamp': [], 'value': [], 'site': [], 'source': [], 'variable': []}
    
    if (nRow>0) and (nCol>3):
        for iCol in range(3,nCol):
            
            head = header[iCol]
            
            #print('** Column: '+head)
        
            # SOURCE
            sourcename = source_type+'.'+head.split()[0]
                
            sns = sourcename.split(".")
            plcprog = sns[-2]
            plcvar = sns[-1]
        
            source = [ plcprog for x in range(nRow)]
        
            tf= (df_sensors.loc[:,"plcprog"]==plcprog) & (df_sensors.loc[:,"plcvar"]==plcvar)
            # ADD CHECK - ONLY ONE ENTRY SHOULD BE FOUND
            if tf.any():
            
                #print("*** Found a match for "+head)    
                #print("*** Transformation ... ")    
                sensor = df_sensors.loc[tf]
            
                # SITE
                sitename = df_sensors["site"].iloc[0]
                site = [sitename for x in range(nRow)]
                    
                # VALUE
                value = raw_data.loc[: , header[iCol]] 
        
                # TIMESTAMP
                timestamp = raw_data.loc[:,"Date"]+' '+raw_data.loc[:,"Time"]
            
                # VARNAME
                varname = sensor["varname"].iloc[0]
                variable = [varname for x in range(nRow)]
                #print("*** Transformation done ")    
                # ADD DATA TO DATAFRAME
                #print("*** Appending data ... ")   
                dictData['timestamp'].extend(timestamp.values)
                dictData['value'].extend(value)
                dictData['site'].extend(site)
                dictData['source'].extend(source)
                dictData['variable'].extend(variable)
                #data_col = pandas.DataFrame( {\
                #                              'timestamp':timestamp, \
                #                              'value': value, \
                #                              'site':site, \
                #                              'source':source, \
                #                              'variable':variable \
                #                             })
                #data =pandas.concat([data,data_col])
                #print("*** Appending data done ")    
                
            else:
                print("*** Could not find known match for "+head)       
            
    
    ## ---> 6.) write file
    dfData = pandas.DataFrame(dictData)
    print('* Writing file: '+output_file)
    dfData.to_csv(output_file, sep=";", index=False)
    print('* Writing file done ')
    
    
    print('* Done! ')
    print('==============================================================')