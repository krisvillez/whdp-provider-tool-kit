#!/usr/bin/python

pattern = 'data-logfileGr*.csv.raw'

###################
### ENVIRONMENT ###
###################

import datetime
import fnmatch
import os
import sys

sys.path.append('../..')

from conversion import convert
from whdpprovider.whdpkit import whdpkit

whdpinst = whdpkit()

###############
###  START  ###
###############
log = whdpinst.log_c_plcgrey
whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

###############
### PROCESS ###
###############

filelist = fnmatch.filter(os.listdir(whdpinst.path_plcgrey_landing), pattern)
#filelist = filelist[0:1]

for file_in in filelist:
    whdpinst.logging(log,'* Converting: '+file_in)
    file_out = 'signal_'+file_in[0:-4]
    convert(os.path.join(whdpinst.path_plcgrey_landing,file_in),\
            os.path.join(whdpinst.path_plcgrey_converted,file_out))
    

###############
###  EMAIL  ###
###############

subject = '[WHDPauto] Transformed plcgrey files succesfully.'
body = 'Files of plcgrey were transformed succesfully.'
whdpinst.sendmail(log,subject,body)

############
### DONE ###
############

whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
whdpinst.logging(log,'* DONE ')

