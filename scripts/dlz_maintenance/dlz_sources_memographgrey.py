#!/usr/bin/python

filename = 'memographgrey_config.xlsx'
description = 'memographgrey'

##################
###  packages  ### 
##################
import datetime
import os
import pandas
import shutil
import sys

sys.path.append('../..')

from whdpprovider.whdpkit import whdpkit
###################
### ENVIRONMENT ###
###################

whdpinst = whdpkit()

###############
###  START  ###
###############
log = whdpinst.log_dlz_plcgrey
whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

########################################################################
### Find data location and copy  ### 
########################################################################

file_source  = os.path.join(whdpinst.path_manual,filename)
file_source_OK  = os.path.isfile(file_source)

whdpinst.logging(log,'* '+file_source+': '+str(file_source_OK))

##############################
### setup folder structure ### 
##############################

#path_homedir = os.getcwd()
#path_dlz = os.path.join(path_homedir,'dlz')
path_data = whdpinst.path_data_target #os.path.join(path_dlz,'data')

whdpinst.dlzinit(log)

##################
### get file   ### 
##################

whdpinst.logging(log,'* Copying: '+str(filename)+' ..')

file_target= os.path.join(whdpinst.path_manual_archive,filename)
shutil.copy2(file_source, file_target)

whdpinst.logging(log,'* Copying: '+str(filename)+' done')

##################### 
### source type   ### 
##################### 

# Get basic description
dfSourceType = pandas.read_excel(io =file_target,sheet_name ='source_type',header =0, nrows=2)
dfSourceType = dfSourceType.astype(str)

sourcetype = dfSourceType.iloc[0]

whdpinst.logging(log,'* Adding sourcetype: '+str(sourcetype["name"])+' ..')

# Get special values
dfSpecialValue = pandas.read_excel(io =file_target,sheet_name ='source_type',header =3,converters={'categorical_value':str,'numerical_value':str,'special_description':str},keep_default_na=False)
nSpecialValue = dfSpecialValue.shape[0]

whdpinst.logging(log,'** Adding folder for sourcetype: '+str(sourcetype["name"])+' ..')

# Add folder for sourcetype if needed
path_sourcetype = os.path.join(path_data,sourcetype["name"])
whdpinst.addfolder(path_sourcetype)
    
whdpinst.logging(log,'** Adding folder for sourcetype: '+str(sourcetype["name"])+' done')
    
# ---------------------------------
#    Create/overwrite YAML file
# ---------------------------------

whdpinst.logging(log,'** Writing YAML file for sourcetype: '+str(sourcetype["name"])+' ..')

targetfile = os.path.join(path_sourcetype,'source_type.yaml')
targetfile_txt = os.path.join(path_sourcetype,'source_type.yaml.txt')

# --- remove old ---
whdpinst.removefile(log,targetfile_txt)
    
# --- create new ---
whdpinst.logging(log,'*** Create: '+targetfile_txt+' ..')

indent = '    '
line0 = ''
line1 = 'name: '+str(sourcetype["name"])
line2 = 'description: '+str(sourcetype["description"])
whdpinst.logging(targetfile_txt,line0)
whdpinst.logging(targetfile_txt,line1)
whdpinst.logging(targetfile_txt,line2)
    
for iSpecialValue in range(nSpecialValue):
    
    if iSpecialValue==0:
        line3 = 'special_values:'
        whdpinst.logging(targetfile_txt,line3)
    
    special = dfSpecialValue.iloc[iSpecialValue]
    
    line4 = indent+'- '+'categorical_value: '+special["categorical_value"]
    line5 = indent+'  '+'numerical_value: '+special["numerical_value"]
    line6 = indent+'  '+'description: '+special["special_description"]
    whdpinst.logging(targetfile_txt,line4)
    whdpinst.logging(targetfile_txt,line5)
    whdpinst.logging(targetfile_txt,line6)

whdpinst.logging(log,'*** Create: '+targetfile_txt+' done')

# --- remove old ---
whdpinst.removefile(log,targetfile)
    
# --- rename new ---
whdpinst.logging(log,'*** Renaming: '+targetfile+' ..')

os.rename(targetfile_txt, targetfile)

whdpinst.logging(log,'*** Renaming: '+targetfile+' done')
whdpinst.logging(log,'** Writing YAML file for sourcetype: '+str(sourcetype["name"])+' done')
whdpinst.logging(log,'* Adding sourcetype: '+str(sourcetype["name"])+' done')

###############
### source  ### 
###############
df_collect = pandas.DataFrame(columns=['source_type','plcprog', 'plcvar', 'site','description',   'varname'])

for groupindex in range(1,9):
    sheet = "Gr"+str(groupindex)
    df_sheet = pandas.read_excel(io =file_target,sheet_name =sheet,header =0)
    df_sheet = df_sheet.iloc[~(df_sheet.isnull().values.any(axis=1)),:]
    
    df_collect_add = pandas.DataFrame({'source_type': df_sheet.loc[:,'source_type'], \
                                   'plcprog': df_sheet.loc[:,'source'], \
                                   'plcvar': df_sheet.loc[:,'PLC program variable'], \
                                   'site': df_sheet.loc[:,'site_name'], \
                                   'description': df_sheet.loc[:,'description'], \
                                   'varname': df_sheet.loc[:,'variable_name']})

    df_collect = pandas.concat([df_collect,df_collect_add])

df_collect = df_collect.reset_index(drop=True)

nSource = df_collect.shape[0]

for iSource in range(nSource):
    source = df_collect.iloc[iSource]
    
    whdpinst.logging(log,\
                     '* Adding source: '+str(source["source_type"])+'/'+str(source["plcprog"])+' ..')
    
    # CHECK THAT SOURCE_TYPE IS AVAILABLE
    path_sourcetype = os.path.join(path_data,source["source_type"])
    path_sourcetype_OK     = os.path.isdir(path_sourcetype)
    
    if path_sourcetype_OK:
        # SOURCE
        
        # ------------------------------------------
        # ---  Add folders for source if needed  ---
        # ------------------------------------------
        path_source  = os.path.join(path_sourcetype,source["plcprog"])
        path_source_raw  = os.path.join(path_source,"raw_data")
        
        whdpinst.addfolder(path_source)
        whdpinst.addfolder(path_source_raw)
        
        # ---------------------------------
        #    Create/overwrite YAML file
        # ---------------------------------
        whdpinst.logging(log,\
                         '** Writing YAML file for source: '+str(source["plcprog"])+' ..')
        
        targetfile = os.path.join(path_source,'source.yaml')
        targetfile_txt = os.path.join(path_source,'source.yaml.txt')
        
        # --- remove old ---
        whdpinst.removefile(log,targetfile_txt)
            
        # --- create new ---
        whdpinst.logging(log,'*** Create: '+targetfile_txt+' ..')
        
        line0 = 'name: '+str(source["plcprog"])
        line1 = 'description: '+str(source["description"])
        line2 = 'serial: '+str(source["plcprog"])+'fakeserial'
        line3 = 'manufacturer: WAGO-Eawag'
        whdpinst.logging(targetfile_txt,line0)
        whdpinst.logging(targetfile_txt,line1)
        whdpinst.logging(targetfile_txt,line2)
        whdpinst.logging(targetfile_txt,line3)
        
        whdpinst.logging(log,\
                         '*** Create: '+targetfile_txt+' done')

        # --- remove old ---
        whdpinst.removefile(log,targetfile)
    
        # --- rename new ---
        whdpinst.logging(log,'*** Renaming: '+targetfile+' ..')
        
        os.rename(targetfile_txt, targetfile)
        
        whdpinst.logging(log,'*** Renaming: '+targetfile+' done')
        whdpinst.logging(log,\
                         '** Writing YAML file for source: '+str(source["plcprog"])+' ..')
        
    else:
        whdpinst.logging(log,\
                         'ERROR - Adding: '+str(source["source_type"])+'.'+str(source["plcprog"])\
                         +': failed - could not find matching source_type.')
        
        subject = '[WHDPauto-ERROR] Adding: '+str(source["source_type"])+'.'+str(source["plcprog"])\
                         +': failed - could not find matching source_type.'
        body = 'ERROR dlz_plcgrey.py line 224.'
        whdpinst.sendmail(log,subject,body)
        
    whdpinst.logging(log,\
                     '* Adding source: '+str(source["source_type"])+'/'+str(source["plcprog"])+' done')
    

############
### DONE ###
############

subject = '[WHDPauto] Checked DLZ for '+description+'.'
body = 'Checked '+filename+' for new signals.'
whdpinst.sendmail(log,subject,body)

whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
whdpinst.logging(log,'* DONE ')