#!/usr/bin/python

filename = 'whdp_table_variable.xlsx'
SheetName = 'variable'

##################
###  packages  ### 
##################
import datetime
import os
import pandas
import shutil
import sys

sys.path.append('../..')

from whdpprovider.whdpkit import whdpkit
from pandas.util.testing import assert_frame_equal

###################
### ENVIRONMENT ###
###################

whdpinst = whdpkit()

###############
###  START  ###
###############
log = whdpinst.log_dlz_variable
whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))


########################################################################
### Find data location and copy  ### 
########################################################################

file_archive = os.path.join(whdpinst.path_manual_archive,filename) 
file_source  = os.path.join(whdpinst.path_manual,filename)

file_source_OK  = os.path.isfile(file_source)

whdpinst.logging(log,'* '+file_source+': '+str(file_source_OK))

##############################
### setup folder structure ### 
##############################

whdpinst.dlzinit(log)

############################
### Read data in archive ### 
############################

FileCopyAvailable = os.path.isfile(file_archive)

if FileCopyAvailable:
    
    whdpinst.logging(log,'* Reading files .. ')
    df_variable_old = pandas.read_excel(io =file_archive,sheet_name =SheetName,header =0)
    whdpinst.logging(log,'* Reading files done')
     
    WriteNewFile = False
    
else:
    whdpinst.logging(log,'* Entirely new file')
    WriteNewFile = True 

########################################################################
### Copy files                                                 ### 
########################################################################

whdpinst.logging(log,'* Copying Excel files ..')
shutil.copy2(file_source, file_archive)
whdpinst.logging(log,'* Copying Excel files done')

#################
### Read data ### 
#################

whdpinst.logging(log,'* Reading files .. ')    
df_variable_new = pandas.read_excel(io =file_archive,sheet_name =SheetName,header =0)
whdpinst.logging(log,'* Reading files done')

###################################
### CHECK IF THERE ARE CHANGES  ### 
###################################

###################################
### CHECK IF THERE ARE CHANGES  ### 
###################################

whdpinst.logging(log,'* Checking for changed results .. ')

if FileCopyAvailable:
    unchanged = df_variable_new.equals(df_variable_old)

    if unchanged:
        whdpinst.logging(log,'** No changes in variable table')
        WriteNewFile = False
    else:
        whdpinst.logging(log,'** Some changes in variable table')
        WriteNewFile = True
    
whdpinst.logging(log,'* Checking for changed results done')

##############
### OUTPUT ### 
##############
if WriteNewFile:
    
    # Store dataframe as file
    targetfile     = os.path.join(whdpinst.path_data_target,'variables.yaml')
    targetfile_txt = os.path.join(whdpinst.path_data_target,'variables.yaml.txt')
    
    whdpinst.logging(log,'* Writing: '+targetfile_txt+' ..')
    
    # ---------------
    # ---  email  ---
    # ---------------
    subject = '[WHDPauto] Modified variable table.'
    body = 'Created new variable table at: '+targetfile+' . \n Please check the DLZ and update the operational landing zone.'
    whdpinst.sendmail(log,subject,body)
    
    # ------------------
    # --- remove old ---
    # ------------------
    whdpinst.removefile(log,targetfile_txt)
    whdpinst.removefile(log,targetfile)
    
    # ------------------
    # --- write new  ---
    # ------------------
    
    indent = '    '
    line0 = '-'
    line4=''
        
    for iRow in range(df_variable_new.shape[0]):
        
        whdpinst.logging(log,\
                         '** Working on: '+df_variable_new["name"].iloc[iRow]+' ..')
        
        line1 = indent+'name: '+str(df_variable_new["name"].iloc[iRow])
        line2 = indent+'unit: '+str(df_variable_new["unit"].iloc[iRow])
        line3 = indent+'description: '+str(df_variable_new["description"].iloc[iRow])
        
        # -- adding output line by line
        whdpinst.logging(targetfile_txt,line0)
        whdpinst.logging(targetfile_txt,line1)
        whdpinst.logging(targetfile_txt,line2)
        whdpinst.logging(targetfile_txt,line3)
        whdpinst.logging(targetfile_txt,line4)
        # -- adding output line by line done
        
        whdpinst.logging(log,\
                         '** Working on: '+df_variable_new["name"].iloc[iRow]+' done')
        

    whdpinst.logging(log,'* Writing: '+targetfile_txt+' done')
    
    # --- rename new ---
    whdpinst.logging(log,'* Renaming: '+targetfile+' ..')
    os.rename(targetfile_txt, targetfile)
    whdpinst.logging(log,'* Renaming: '+targetfile+' done')

else:
    whdpinst.logging(log,'* No files to write')
    
############
### DONE ###
############

subject = '[WHDPauto] Checked variable table.'
body = 'Variable table was checked.'
whdpinst.sendmail(log,subject,body)

whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
whdpinst.logging(log,'* DONE ')