#!/usr/bin/python

filename = 'whdp_table_site.xlsx'

##################
###  packages  ### 
##################
import datetime
import os
import pandas
import shutil
import sys

sys.path.append('../..')

from whdpprovider.whdpkit import whdpkit
from pandas.util.testing import assert_frame_equal

###################
### ENVIRONMENT ###
###################

whdpinst = whdpkit()

###############
###  START  ###
###############
log = whdpinst.log_dlz_site
whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))


########################################################################
### Find data location and copy  ### 
########################################################################

file_source  = os.path.join(whdpinst.path_manual,filename)
file_source_OK  = os.path.isfile(file_source)

whdpinst.logging(log,'* '+file_source+': '+str(file_source_OK))

##############################
### setup folder structure ### 
##############################

path_sites = os.path.join(whdpinst.path_dlz,'sites')

whdpinst.dlzinit(log)

##################
### get file   ### 
##################

whdpinst.logging(log,'* Copying: '+str(filename)+' ..')

file_target= os.path.join(whdpinst.path_manual_archive,filename)
shutil.copy2(file_source, file_target)

whdpinst.logging(log,'* Copying: '+str(filename)+' done')


###############
### sites   ### 
###############

# Get basic description
dfSites = pandas.read_excel(io =file_target,sheet_name ='data',header =0)
dfSites.fillna('N/A', inplace=True)
dfSites = dfSites.astype(str)

nSite = dfSites.shape[0]

for iSite in range(nSite):
    site = dfSites.iloc[iSite]
    
    SiteName = site["Name"]
    whdpinst.logging(log,'* Adding site: '+str(SiteName)+' ..')

    # ------------------------------------------
    # ---  Add folders for site if needed  ---
    # ------------------------------------------
    
    path_site    = os.path.join(path_sites,SiteName)
    whdpinst.addfolder(path_site)
    path_site_images  = os.path.join(path_site,"images")
    whdpinst.addfolder(path_site_images)
    
    # ---------------------------------
    #    Create/overwrite YAML file
    # ---------------------------------
    
    whdpinst.logging(log,'** Writing YAML file for site: '+str(SiteName)+' ..')
        
    targetfile = os.path.join(path_site,'site.yaml')
    targetfile_txt = os.path.join(path_site,'site.yaml.txt')
    
    # --- remove old ---
    whdpinst.removefile(log,targetfile_txt)
            
    # --- create new ---
    whdpinst.logging(log,'*** Create: '+targetfile_txt+' ..')
    
    print(site)
    line0 = 'name: '+str(SiteName)
    line1 = 'description: '+str(site["Description"])
    line2 = 'unit: '+str(site["Unit"])
    line3 = 'area: '+str(site["Area"])
    line4 = 'setup: '+str(site["Setup"])
    line5 = 'component: '+str(site["Component"])
    line6 = 'status: '+str(site["Status"])
    
    whdpinst.logging(targetfile_txt,line0)
    whdpinst.logging(targetfile_txt,line1)
    whdpinst.logging(targetfile_txt,line2)
    whdpinst.logging(targetfile_txt,line3)
    whdpinst.logging(targetfile_txt,line4)
    whdpinst.logging(targetfile_txt,line5)
    whdpinst.logging(targetfile_txt,line6)
    
    whdpinst.logging(log,'*** Create: '+targetfile_txt+' done')

    # --- remove old ---
    whdpinst.removefile(log,targetfile)
    
    # --- rename new ---
    whdpinst.logging(log,'*** Renaming: '+targetfile+' ..')
    os.rename(targetfile_txt, targetfile)
    whdpinst.logging(log,'*** Renaming: '+targetfile+' done')
    
    whdpinst.logging(log,'** Writing YAML file for site: '+str(SiteName)+' done')
    whdpinst.logging(log,'* Adding site: '+str(SiteName)+' done')

############
### DONE ###
############

subject = '[WHDPauto] Checked sites.'
body = 'Sites in DLZ structure were checked.'
whdpinst.sendmail(log,subject,body)

whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
whdpinst.logging(log,'* DONE ')
