#!/usr/bin/python

filename = 'LabSamples2019.xlsx'
SheetName = 'person_db'
whdpfile_person = "persons.yaml"

##################
###  packages  ### 
##################
import datetime
import os
import pandas
import shutil
import sys
import yaml

sys.path.append('../..')

from whdpprovider.whdpkit import whdpkit
from pandas.util.testing import assert_frame_equal

###################
### ENVIRONMENT ###
###################

whdpinst = whdpkit()

###############
###  START  ###
###############
log = whdpinst.log_dlz_person
whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

########################################################################
### Find data location   ### 
########################################################################

file_archive = os.path.join(whdpinst.path_lab_local_archive,filename) 
file_archive_OK  = os.path.isfile(file_archive)
whdpinst.logging(log,'** '+file_archive+': '+str(file_archive_OK))

file_target = os.path.join(whdpinst.path_lab_target,whdpfile_person) 
file_target_OK = os.path.isfile(file_target)
whdpinst.logging(log,'** '+file_target+': '+str(file_target_OK))

##############################
### setup folder structure ### 
##############################

whdpinst.dlzinit(log)

########################################################################
### Find person table and read  ### 
########################################################################

WriteNewFile = False
if file_archive_OK:
    ##########################################################
    ### Read current yaml file in development landing zone ### 
    ##########################################################
    whdpinst.logging(log,'* Reading YAML file .. ')
    with open(file_target, 'r') as stream:
        try:
            lst = yaml.load(stream)
            person_whdp = pandas.DataFrame(lst)
        except yaml.YAMLError as exc:
            whdpinst.logging(log,'ERROR: could not read '+file_target)
            
            subject = '[WHDPauto-ERROR] could not read '+file_target
            body = 'ERROR c_lab.py line 77.'
            whdpinst.sendmail(log,subject,body)
    whdpinst.logging(log,'* Reading YAML file done .. ')
    
    ###############################
    ### Read current Excel file ### 
    ###############################
    whdpinst.logging(log,'* Reading Excel file .. ')
    person_excel = pandas.read_excel(io =file_archive,sheet_name =SheetName,header =0)
    person_excel["alias"] = person_excel["alias"].astype('str')
    person_excel["alias"] = person_excel["alias"].apply(lambda x: x.lower() )
    person_excel["dropdown"] = person_excel["dropdown"].astype('str')
    person_excel["dropdown"] = person_excel["dropdown"].apply(lambda x: x.lower() )
    whdpinst.logging(log,'* Reading Excel file done')
    
    #######################################
    ### Process lab teams in Excel file ### 
    #######################################
    whdpinst.logging(log,'* Processing teams in Excel file .. ')
    for iRow in range(person_excel.shape[0]):
        if person_excel["alias"].iloc[iRow]==person_excel["dropdown"].iloc[iRow]:
            pass
        else:
            acronym = str(person_excel["dropdown"].iloc[iRow])
            acronym = acronym.replace(' ','') 
            lst = acronym.split(',')
            composed_alias = ''
            for p in range(len(lst)):
                index = (person_excel["dropdown"]==lst[p])
                if index.any():
                    thisperson = person_excel[index].iloc[0]
                    if p==0:
                        composed_alias = composed_alias+thisperson["alias"]
                    else:
                        composed_alias = composed_alias+'+'
                        composed_alias = composed_alias+thisperson.loc["alias"]
                else:
                    whdpinst.logging(log,'ERROR: Could not find '+str(lst[p])+' in Excel data.')
                    subject = '[WHDPauto-ERROR] Could not find '+str(lst[p])+' in Excel data.'
                    body = 'ERROR c_lab.py line 124.'
                    whdpinst.sendmail(log,subject,body)
        
            person_excel["alias"].iloc[iRow] = composed_alias
    whdpinst.logging(log,'* Processing teams in Excel file done ')
    
    #####################
    ### New YAML file ### 
    #####################
    
    # ------------------------
    # --- create filenames ---
    # ------------------------
    targetfile     = os.path.join(whdpinst.path_lab_target,'persons.yaml')
    targetfile_txt = os.path.join(whdpinst.path_lab_target,'persons.yaml.txt')
    
    whdpinst.logging(log,'* Making new YAML file '+targetfile_txt+' .. ')
    # ------------------
    # --- remove old ---
    # ------------------
    whdpinst.removefile(log,targetfile_txt)
    whdpinst.removefile(log,targetfile)
    
    # ------------------
    # --- write new  ---
    # ------------------
    whdpinst.logging(log,'** Working on people in spreadsheet')
    indent = '    '
    for iRow in range(person_excel.shape[0]):
        whdpinst.logging(log,'*** Working on: '+person_excel["dropdown"].iloc[iRow]+' ..')
        
        AnyERROR = False

        line0 = '-'
        line4=''
        if person_excel["alias"].iloc[iRow]==person_excel["dropdown"].iloc[iRow]:
            line1 = indent+'abbrev: '+person_excel["alias"].iloc[iRow]
            line2 = indent+'name: '+person_excel["name"].iloc[iRow]
            line3 = indent+'department: '+person_excel["department"].iloc[iRow]
        else:
            acronym = str(person_excel["dropdown"].iloc[iRow])
            acronym = acronym.replace(' ','') 
            lst = acronym.split(',')
            line1 = indent+'abbrev: '
            line2 = indent+'name: '
            line3 = indent+'department: '
            for p in range(len(lst)):
                index = (person_excel["dropdown"]==lst[p])
                if index.any():
                    thisperson = person_excel[index].iloc[0]
                    if p==0:
                        line3 = line3+thisperson.loc["department"]
                    else:
                        line1 = line1+'+'
                        line2 = line2+', '
                    line1 = line1+thisperson.loc["alias"]
                    line2 = line2+thisperson.loc["name"]
                else:
                    whdpinst.logging(log,'ERROR: could not find '+lst[p])
                    AnyERROR = True
                    
                    subject = '[WHDPauto-ERROR] could not find '+lst[p]
                    body = 'ERROR dlz_person.py line 161.'
                    whdpinst.sendmail(log,subject,body)
        
        if (not AnyERROR):
            whdpinst.logging(targetfile_txt,line0)
            whdpinst.logging(targetfile_txt,line1)
            whdpinst.logging(targetfile_txt,line2)
            whdpinst.logging(targetfile_txt,line3)
            whdpinst.logging(targetfile_txt,line4)
        
        whdpinst.logging(log,'*** Working on: '+person_excel["dropdown"].iloc[iRow]+' done')
        
    whdpinst.logging(log,'** Working on people in spreadsheet done')
    
    whdpinst.logging(log,'** Working on pre-existing people')
    if file_target_OK:
        
        for iRow in range(person_whdp.shape[0]):
        
            strAlias = person_whdp["abbrev"].iloc[iRow]
            whdpinst.logging(log,'*** Working on: '+strAlias+' ..')
        
            AnyERROR = False
            
            line0 = '-'
            line4=''
            if not (strAlias in person_excel['alias'].values):
                line1 = indent+'abbrev: '+strAlias
                line2 = indent+'name: '+person_whdp["name"].iloc[iRow]
                line3 = indent+'department: '+person_whdp["department"].iloc[iRow]
            
                if (not AnyERROR):
                    whdpinst.logging(targetfile_txt,line0)
                    whdpinst.logging(targetfile_txt,line1)
                    whdpinst.logging(targetfile_txt,line2)
                    whdpinst.logging(targetfile_txt,line3)
                    whdpinst.logging(targetfile_txt,line4)
        
            whdpinst.logging(log,'*** Working on: '+strAlias+' done')
        
    whdpinst.logging(log,'** Working on pre-existing people done')
    
    # --- rename new ---
    whdpinst.logging(log,'** Renaming: '+targetfile+' ..')
    os.rename(targetfile_txt, targetfile)
    whdpinst.logging(log,'** Renaming: '+targetfile+' done')
    
    # ---------------
    # ---  email  ---
    # ---------------
    subject = '[WHDPauto] Modified person table.'
    body = 'Created new person table at: '+targetfile+' . \n Please check the DLZ, and update the operational landing zone.'
    whdpinst.sendmail(log,subject,body)
    
    whdpinst.logging(log,'* Making new YAML file '+targetfile_txt+' done ')
    
else:
    whdpinst.logging(log,'* No files to write')
    
###############
###  EMAIL  ###
###############

subject = '[WHDPauto] Checked person table.'
body = 'Person table was checked.'
whdpinst.sendmail(log,subject,body)

############
### DONE ###
############


whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
whdpinst.logging(log,'* DONE ')
