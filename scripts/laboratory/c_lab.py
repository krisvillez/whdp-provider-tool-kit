#!/usr/bin/python

name_sheet  = "NEST_GW"
name_sheet_extra  = "extra_NEST_GW"
whdpfile_person = "persons.yaml"
person_sheet = 'person_db'
years = [2018,2019]
#years = [2019]

###################
### ENVIRONMENT ###
###################

import datetime
import pandas as pd
import os
import shutil
import yaml
import sys

idx       = pd.IndexSlice

sys.path.append('../..')

from whdpprovider.whdpkit import whdpkit

whdpinst = whdpkit()

###############
###  START  ###
###############
log = whdpinst.log_c_lab
whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

#cols_sample = ['ProjectID','Number','Room','Area','Setup','Setupcomponent','Date','Time','Samplingmethod','Filteratsampling','Dilutionatsampling','WhoS','Comment','Communication','Finished','WhoA']

#cols_sample_complete = {'ProjectID','Number','Room','Area','Setup','Setupcomponent','date','time','Samplingmethod','Filteratsampling','Dilutionatsampling','WhoS','Finished','WhoA'} 
#cols_result = ['SampleID','Comment','DOC','TOC','TIC','Ntot','Cl','NO2N','NO3N','PO4P','SO4','Na','NH4N','K','Ca','Mg','Ptot','CODsol','CODtot','Alkalinity','Conductivity','pH','DissolvedOxygen','Turbidity','BOD5','TSS','VSS','TCC','ICC','ATP']
varnamesExcel=['DOC','TOC','TIC','Ntot','Cl','NO2N','NO3N','PO4P','SO4','Na','NH4N','K','Ca','Mg','Ptot','CODsol','CODtot','Alkalinity','Conductivity','pH','DissolvedOxygen','Turbidity','BOD5','TSS','VSS','TCC','ICC','ATP']
varnamesWHDP = ['DOC','TOC','TIC','Ntot','Cl','NO2N','NO3N','PO4P','SO4','Na','NH4N','K','Ca','Mg','Ptot','CODsol','CODtot','Alk','Cond','pH','DOconc','Turb','BOD5','TSS','VSS','TCC','ICC','ATP']

required = ['lab_identifier','sample_identifier','variable','filter_lab','method_lab','value_lab','person_abbrev_lab','timestamp_end_lab','site','person_abbrev_sample','method_sample','filter_sample','timestamp_sample']

########################################################################
### Folder location checks  ### 
########################################################################
log = whdpinst.log_c_lab
whdpinst.logging(log,'* Checking folder and file accessibility')

path_lab_local_archive_OK = os.path.isdir(whdpinst.path_lab_local_archive)
path_lab_local_OK = os.path.isdir(whdpinst.path_lab_local)
path_lab_operational_OK = os.path.isdir(whdpinst.path_lab_operational)
whdpinst.logging(log,'** '+whdpinst.path_lab_local_archive+': '+str(path_lab_local_archive_OK))
whdpinst.logging(log,'** '+whdpinst.path_lab_local+': '+str(path_lab_local_OK))
whdpinst.logging(log,'** '+whdpinst.path_lab_operational+': '+str(path_lab_operational_OK))

DF_SAMPLE = pd.DataFrame()
DF_RESULT = pd.DataFrame(dtype=str)
DF_PERSON = pd.DataFrame(dtype=str)


########################################################################
### Person data input  ### 
########################################################################
whdpinst.logging(log,'* Person data reading ..')

sourcefile_person = os.path.join(whdpinst.path_lab_operational,whdpfile_person) 
SourcePersonFileOK = os.path.isfile(sourcefile_person)

whdpinst.logging(log,'** '+sourcefile_person+': '+str(SourcePersonFileOK))

with open(sourcefile_person, 'r') as stream:
    try:
        lst = yaml.load(stream)
        person_whdp = pd.DataFrame(lst)
        person_whdp.drop_duplicates(subset='abbrev',keep='first', inplace=True)
    except yaml.YAMLError as exc:
        whdpinst.logging(log,'ERROR: could not read '+sourcefile_person)
        
        subject = '[WHDPauto-ERROR] could not read '+sourcefile_person
        body = 'ERROR c_lab.py line 77.'
        #whdpinst.sendmail(log,subject,body)

whdpinst.logging(log,'* Person data reading done')
########################################################################
### Raw data input ### 
########################################################################
whdpinst.logging(log,'* Excel data reading ..')

for iYear, numYear in enumerate(years):
    
    whdpinst.logging(log,'** Year: '+str(numYear))
    
    fileSample = 'LabSamples'+str(numYear)+'.xlsx'
    fileResult = 'LabResults'+str(numYear)+'_NEST_GW.xlsx'
    fileSampleExtra = 'LabSamples'+str(numYear)+'_NEST_GW_extra.xlsx'
    fileResultExtra = 'LabResults'+str(numYear)+'_NEST_GW_extra.xlsx'
    
    filepathSample = os.path.join(whdpinst.path_lab_local,fileSample) 
    filepathResult = os.path.join(whdpinst.path_lab_local,fileResult)
    filepathSampleExtra = os.path.join(whdpinst.path_lab_local,fileSampleExtra) 
    filepathResultExtra = os.path.join(whdpinst.path_lab_local,fileResultExtra)

    SourceSampleOK = os.path.isfile(filepathSample)
    SourceResultOK = os.path.isfile(filepathResult)
    SourceSampleExtraOK = os.path.isfile(filepathSampleExtra)
    SourceResultExtraOK = os.path.isfile(filepathResultExtra)
    
    whdpinst.logging(log,'** '+fileSample+': '+str(SourceSampleOK))
    whdpinst.logging(log,'** '+fileResult+': '+str(SourceResultOK))
    whdpinst.logging(log,'** '+fileSampleExtra+': '+str(SourceSampleExtraOK))
    whdpinst.logging(log,'** '+fileResultExtra+': '+str(SourceResultExtraOK))
    
    ########################################################################
    ### Copy files                                                 ### 
    ########################################################################

    whdpinst.logging(log,'** Copying Excel files ..')

    archiveSample = os.path.join(whdpinst.path_lab_local_archive,fileSample) 
    archiveResult = os.path.join(whdpinst.path_lab_local_archive,fileResult)
    archiveSampleExtra = os.path.join(whdpinst.path_lab_local_archive,fileSampleExtra) 
    archiveResultExtra = os.path.join(whdpinst.path_lab_local_archive,fileResultExtra)

    shutil.copy2(filepathSample, archiveSample)
    shutil.copy2(filepathResult, archiveResult)
    shutil.copy2(filepathSampleExtra, archiveSampleExtra)
    shutil.copy2(filepathResultExtra, archiveResultExtra)

    whdpinst.logging(log,'** Copying Excel files done')
    
    ########################## 
    ###  Read person data  ### 
    ##########################
    
    print(archiveSample)
    
    whdpinst.logging(log,'** Reading person data .. ')
    
    dfPerson = pd.read_excel(io =archiveSample, sheet_name =person_sheet,dtype='str',header =(0),keep_default_na=False,na_values={""},encoding='latin1').astype(str)
    
    DF_PERSON = pd.concat([DF_PERSON, dfPerson], axis=0,sort=False)
    
    ########################## 
    ###  Read sample data  ### 
    ##########################
    
    whdpinst.logging(log,'** Reading sample data .. ')
    dfSample = pd.read_excel(io =archiveSample,\
                             sheet_name =name_sheet,\
                             dtype='str',\
                             header =(4,5),\
                             keep_default_na=False,\
                             na_values={""},\
                             skiprows=range(6, 8),encoding='latin1')
    xProjectAndNumberOK = ~(pd.isnull(dfSample.iloc[:,0:2]).any(axis=1))
    dfSample = dfSample.loc[xProjectAndNumberOK,:]
    DF_SAMPLE = pd.concat([DF_SAMPLE, dfSample], axis=0,sort=False)
    
    dfSampleExtra = pd.read_excel(io =archiveSampleExtra,\
                                  sheet_name =name_sheet_extra,\
                                  dtype='str',\
                                  header =(4,5),\
                                  keep_default_na=False,\
                                  na_values={""},\
                                  skiprows=range(6, 8),encoding='latin1')
    xProjectAndNumberOK = ~(pd.isnull(dfSampleExtra.iloc[:,0:2]).any(axis=1))
    dfSampleExtra = dfSampleExtra.loc[xProjectAndNumberOK,:]
    DF_SAMPLE = pd.concat([DF_SAMPLE, dfSampleExtra], axis=0,sort=False)
    
    whdpinst.logging(log,'** Reading sample data done')
    
    
    ########################## 
    ###  Read result data  ### 
    ##########################
    
    whdpinst.logging(log,'** Reading result data .. ')

    dfResult = pd.read_excel(io =archiveResult,\
                                 sheet_name =name_sheet,\
                                 dtype='str',\
                                 header =(4,5),\
                                 keep_default_na=False,\
                                 na_values={""},\
                                 skiprows=range(6, 8),encoding='latin1')
    xProjectAndNumberOK = ~(pd.isnull(dfResult.iloc[:,0:2]).any(axis=1))
    dfResult = dfResult[xProjectAndNumberOK]
    xSomeResultsAvailable = ~(pd.isnull(dfResult.iloc[:,4:]).all(axis=1))
    dfResult = dfResult[xSomeResultsAvailable]
    DF_RESULT = pd.concat([DF_RESULT, dfResult], axis=0,sort=False)
    
    dfResultExtra = pd.read_excel(io =archiveResultExtra,\
                                      sheet_name =name_sheet_extra,\
                                      dtype='str',\
                                      header =(4,5),\
                                      keep_default_na=False,\
                                      na_values={""},\
                                      skiprows=range(6, 8),encoding='latin1')
    xProjectAndNumberOK = ~(pd.isnull(dfResultExtra.iloc[:,0:2]).any(axis=1))
    dfResultExtra = dfResultExtra[xProjectAndNumberOK]
    xSomeResultsAvailable = ~(pd.isnull(dfSampleExtra.iloc[:,4:]).all(axis=1))
    dfResultExtra = dfResultExtra[xSomeResultsAvailable]
    
    DF_RESULT = pd.concat([DF_RESULT, dfResultExtra], axis=0,sort=False)
    
    whdpinst.logging(log,'** Reading result data done')

DF_RESULT.rename(columns=lambda x: str(x), inplace=True)
DF_RESULT.rename(columns=lambda x: x.replace(' ','').replace('-','').replace('(','').replace(')',''), inplace=True)

whdpinst.logging(log,'* Excel data reading done')
DF_SAMPLE=DF_SAMPLE.drop('Expected value of measured variable', axis=1)
DF_SAMPLE=DF_SAMPLE.drop('ConcatID', axis=1)
DF_SAMPLE=DF_SAMPLE.drop('Number of options', axis=1)

############################################
###   Filter out incomplete information  ### 
############################################

whdpinst.logging(log,'* Data filtering')

# ---- Results: 
# compute sample ID + compute SampleID 
# + check if any rows in DF_RESULT are missing in DF_RESULT
# + compute SampleID with repetition indicator 
DF_RESULT.rename(columns=lambda x: x.replace(' ','').replace('(','').replace(')',''), inplace=True)
DF_RESULT['SampleID', 'SampleID'] = DF_RESULT["SampleID"]["ProjectID"]+DF_RESULT["SampleID"]["Number"]
DF_RESULT['SampleID', 'Repetition'] = 'NaN'
for sample_id in DF_RESULT['SampleID', 'SampleID'].unique():
    xMatch = DF_RESULT['SampleID', 'SampleID'] == sample_id
    DF_RESULT['SampleID', 'Repetition'].loc[xMatch] = range(sum(xMatch))
    
DF_RESULT.sort_values([('SampleID', 'ProjectID'),('SampleID', 'Number'),('SampleID', 'Repetition')], ascending=False).astype(str) 
    
# ---- Samples: drop columns + compute SampleID + remove duplicates
DF_SAMPLE.drop("Number of options",axis=1,level=0, inplace=True)
DF_SAMPLE.drop("Expected value of measured variable",axis=1,level=0, inplace=True)
DF_SAMPLE.rename(columns=lambda x: x.replace(' ','').replace('(','').replace(')',''), inplace=True)
DF_SAMPLE['SampleID', 'SampleID'] = DF_SAMPLE["SampleID"]["ProjectID"]+DF_SAMPLE["SampleID"]["Number"]
DF_SAMPLE.drop_duplicates(subset=[('SampleID', 'ProjectID'),('SampleID', 'Number')],keep='first', inplace=True)
DF_SAMPLE.sort_values([('SampleID', 'ProjectID'),('SampleID', 'Number')], ascending=False)
df1=DF_SAMPLE.drop("Comment",axis=1,level=0).drop("Communication",axis=1,level=0)


# ---- Check if all values of SampleID in DF_RESULT appear as SampleID in DF_SAMPLE, send email if test fails
xAllSamplesDefined = DF_RESULT['SampleID', 'SampleID'].isin(DF_SAMPLE['SampleID', 'SampleID']).all()
'''
if (not xAllSamplesDefined):
    subject = '[WHDPauto-WARNING] Laboratory: There are SampleID in DF_RESULT that are not available in DF_SAMPLE.'
    body = 'WARNING c_lab.py line 245.'
    whdpinst.sendmail(log,subject,body)
'''
 
# ---- Samples: drop rows in DF_SAMPLE which have incomplete information, e.g. analysis is not finished yet
xComplete = (~df1.isna()).all(axis=1)
DF_SAMPLE= DF_SAMPLE[xComplete]

# ---- Samples: drop rows in DF_SAMPLE which have no match in DF_RESULT
DF_SAMPLE =DF_SAMPLE.loc[DF_SAMPLE['SampleID', 'SampleID'].isin(DF_RESULT['SampleID', 'SampleID'])]

# ---- Results: drop rows in DF_RESULT which have no match in DF_SAMPLE
DF_RESULT =DF_RESULT.loc[DF_RESULT['SampleID', 'SampleID'].isin(DF_SAMPLE['SampleID', 'SampleID'])]


DF_PERSON.replace(' ', '_', regex=True,inplace=True)
DF_RESULT.replace(' ', '_', regex=True,inplace=True)
DF_SAMPLE.replace(' ', '_', regex=True,inplace=True)

whdpinst.logging(log,'* Data filtering done')

########################################################
###  Check whether persons are identified correctly  ### 
########################################################

whdpinst.logging(log,'* Person checking')

DF_PERSON.drop_duplicates(subset='dropdown',keep='last', inplace=True)
DF_PERSON['dropdown']=DF_PERSON['dropdown'].apply(lambda x: str(x).lower())  
DF_SAMPLE['Analysis', 'WhoA'] = DF_SAMPLE['Analysis', 'WhoA'].apply(lambda x: x.lower()) 
DF_SAMPLE['Sample', 'WhoS']=DF_SAMPLE['Sample', 'WhoS'].apply(lambda x: x.lower()) 

# check for missing dropdown elements:
WhoS = DF_SAMPLE['Sample', 'WhoS'] 
xDropdownWhoSampling = WhoS.isin(DF_PERSON['dropdown'])
DropdownWhoSamplingOK = xDropdownWhoSampling.all(axis=0)
print(DropdownWhoSamplingOK)

WhoA = DF_SAMPLE['Analysis', 'WhoA']
xDropdownWhoAnalysis = WhoA.isin(DF_PERSON['dropdown'])
DropdownWhoAnalysisOK = xDropdownWhoAnalysis.all(axis=0)

print(DropdownWhoAnalysisOK)
print(WhoA.loc[~xDropdownWhoAnalysis])

# make sure aliases are defined for lab teams
xAliasMissing = DF_PERSON.dropdown.apply(lambda x: len(x.replace('_','').split(','))>1 ) 
DF_PERSON.loc[xAliasMissing, 'alias'] = DF_PERSON.loc[xAliasMissing, 'dropdown']
s = '+'
DF_PERSON.loc[xAliasMissing, 'alias'] =   DF_PERSON.loc[xAliasMissing, 'alias'].apply(lambda x:  s.join( [ (DF_PERSON.loc[DF_PERSON['dropdown']==sub,'alias'].values[0]) for sub in x.replace('_','').split(',') ])  )


# check for missing persons:
df_alias = DF_PERSON['alias']
print(df_alias.loc[~df_alias.isin(person_whdp['abbrev'])])
xAllPersonsInWHDP = DF_PERSON['alias'].isin(person_whdp['abbrev']).all(axis=0) 
xAllPersonsInExcel = person_whdp['abbrev'].isin(DF_PERSON['alias']).all(axis=0)

'''
if (not xAllPersonsInExcel):
    subject = '[WHDPauto-WARNING] Laboratory: There are persons in WHDP that are not accounted for in Excel sheets.'
    body = 'WARNING c_lab.py line 245.'
    whdpinst.sendmail(log,subject,body)
'''

whdpinst.logging(log,'* Person checking done')

########################################################
###  Process data  ### 
########################################################
if (not xAllPersonsInWHDP):
    
    whdpinst.logging(log,'[ERROR] Laboratory: There are new persons.')
    
    subject = '[WHDPauto-ERROR] Laboratory: There are new persons.'
    body = 'ERROR c_lab.py line 255.'
    whdpinst.sendmail(log,subject,body)
elif (not (DropdownWhoSamplingOK and DropdownWhoAnalysisOK)):
    
    whdpinst.logging(log,'[ERROR] Invalid dropdown selectors for persons.')
    
    subject = '[WHDPauto-ERROR] Invalid dropdown selectors for persons.'
    body = 'ERROR c_lab.py line 250.'
    whdpinst.sendmail(log,subject,body)
    
else:
    
    # ---------------------------------
    #  Numeric values where necessary
    # ---------------------------------

    whdpinst.logging(log,'* Enforcing numeric and scale in DF_RESULT ..')

    for varname in varnamesExcel:
        varproperties = ['Finalvalue' ,'Dilution' ]
        for varproperty in varproperties:
            
            DF_RESULT[(varname,varproperty)]=DF_RESULT[(varname,varproperty)].apply(pd.to_numeric, errors='ignore')
            xFailed = DF_RESULT[(varname,varproperty)].astype('str').str.startswith('failed', na=False)
            DF_RESULT.loc[xFailed,(varname,varproperty)]=-999999.999999
            
            xLOQ = DF_RESULT[(varname,varproperty)].astype('str').str.startswith('<', na=False)
            
            DF_RESULT.loc[xLOQ,(varname,varproperty)]=DF_RESULT.loc[xLOQ,(varname,varproperty)].apply(lambda x: -float(x[1:]) )
            
            if (varname=='Cond') and (varproperty=='Finalvalue'):
                DF_RESULT.loc[xLOQ,(varname,varproperty)]= DF_RESULT.loc[xLOQ,(varname,varproperty)]/1000
                
            DF_RESULT.loc[xLOQ,(varname,varproperty)]=DF_RESULT.loc[xLOQ,(varname,varproperty)].apply(lambda x: -990000+x )

    whdpinst.logging(log,'* Enforcing numeric and scale in DF_RESULT done')
    
    
    whdpinst.logging(log,'* Data transformation .. ')
    
    # --------------------------------------------------------------------------------
    #  Loop over all target variables and add corresponding data to a new dataframe
    # --------------------------------------------------------------------------------

    df_out = pd.DataFrame(columns=['lab_identifier','sample_identifier','variable','filter_lab','dilution_lab','method_lab','value_lab','description_lab','person_abbrev_lab','timestamp_start_lab','timestamp_end_lab','site','person_abbrev_sample','method_sample','filter_sample','dilution_sample','timestamp_sample','description_sample'],dtype=str)
    row_out = 0

    for varname in varnamesWHDP:
        
        whdpinst.logging(log,'** Variable: '+varname+' ..')
        
        varnameLong= varnamesExcel[varnamesWHDP.index(varname)]
        DF_VAR = DF_RESULT.loc[:,['SampleID','Repetition','Comment',varnameLong]]
        xComplete = ~(pd.isnull(DF_VAR.drop(['Comment',(varnameLong,'Dilution')], axis=1)).any(axis=1))
        DF_VAR = DF_VAR[xComplete]
        nValue = DF_VAR.shape[0]
        
        if nValue>0:
            for iValue in range(nValue):
                
                # ELEMENTS FROM RESULTS:
                df_result = DF_VAR.iloc[iValue,:]
                df_result = df_result #.astype(str)
                repetition = df_result[('SampleID','Repetition')]
                sample_identifier = df_result[('SampleID','SampleID')]
                
                # 'lab_identifier'
                lab_identifier = sample_identifier+'_'+str(repetition).zfill(3)+'_lab'+varname
                
                # 'variable' 
                variable = 'bq_'+varname.lower()
                # 'filter_lab'
                filter_lab = df_result[(varnameLong,'Filtration')] #.astype(str)
                # 'dilution_lab'
                dilution_lab =df_result[(varnameLong,'Dilution')]
                if pd.isna(dilution_lab):
                    dilution_lab = ''
                # 'method_lab',
                method_lab = df_result[(varnameLong,'Method')]
                # 'value_lab',
                value_lab = df_result[(varnameLong,'Finalvalue')]
                # 'description_lab'
                description_lab = df_result['Comment'].values[0]
                if pd.isna(description_lab):
                    description_lab= ''
                
                # ELEMENTS FROM SAMPLE:
                # find row in df_sample with corresponding project & number:
                df_sample = DF_SAMPLE[DF_SAMPLE[('SampleID','SampleID')]==sample_identifier]
                
                # 'site',
                s = '_'
                site = s.join( [df_sample['Site']['Room'].values[0],\
                       df_sample['Site']['Area'].values[0],\
                       df_sample['Site']['Setup'].values[0],\
                       df_sample['Site']['Component'].values[0]] ) 
                
                # 'timestamp_sample',
                date1=df_sample[('Sample','Date')].values[0]
                date_sample = datetime.datetime.strptime(date1, '%Y-%m-%d_%H:%M:%S').date()
                time_sample = df_sample[('Sample','Time')].values[0]
                #print(sample_identifier)
                #print(df_sample[('Sample','Date')].values[0])
                #print(df_sample[('Sample','Time')].values[0])
                timestamp_sample = date_sample.strftime('%Y-%m-%d')+' '+str(time_sample)
                dt_timestamp_sample = datetime.datetime.strptime(timestamp_sample, '%Y-%m-%d %H:%M:%S')
                
                # 'method_sample',
                method_sample = str(df_sample[('Sample','Samplingmethod')].values[0])
                # 'filter_sample'
                filter_sample = str(df_sample[('Sample','Filteratsampling')].values[0])
                # 'dilution_sample',
                dilution_sample = str(df_sample[('Sample','Dilutionatsampling')].values[0])
                
                # 'person_abbrev_sample'
                person_acro = df_sample[('Sample','WhoS')].values[0]
                person_abbrev_sample = DF_PERSON['alias'].loc[DF_PERSON['dropdown']==person_acro].values[0].replace('+',',')
                
                # 'description_sample'
                description_sample = df_sample['Comment'].values[0]
                if pd.isna(description_sample):
                    description_sample= ''
                
                # 'person_abbrev_lab'
                person_acro = df_sample[('Analysis','WhoA')].values[0]
                person_abbrev_lab = DF_PERSON['alias'].loc[DF_PERSON['dropdown']==person_acro].values[0].replace('+',',')
                
                
                date2=df_sample[('Analysis','Date')].values[0]
                try: # single date entry - as Excel date
                    dt_timestamp_start_lab = datetime.datetime.strptime(date2, '%Y-%m-%d_%H:%M:%S')
                    dt_timestamp_start_lab = max(dt_timestamp_sample,dt_timestamp_start_lab)
                    dt_timestamp_end_lab = dt_timestamp_start_lab.date() + datetime.timedelta(hours=23,minutes=59,seconds=59)
                    pass
                except: # two-date entry - string format
                    strStart = date2[0:5]+date2[11:]
                    strEnd = date2[6:]
                    dt_timestamp_start_lab = datetime.datetime.strptime(strStart, '%d.%m.%Y')
                    dt_timestamp_end_lab = datetime.datetime.strptime(strEnd, '%d.%m.%Y')
                
                timestamp_start_lab = dt_timestamp_end_lab.strftime('%Y-%m-%d %H:%M:%S') 
                timestamp_end_lab = dt_timestamp_end_lab.strftime('%Y-%m-%d %H:%M:%S') 
                
                # ADD DATA TO DATAFRAME
                df_out = df_out.append({'lab_identifier':lab_identifier, \
                                        'sample_identifier':sample_identifier, \
                                        'variable':variable, \
                                        'filter_lab':filter_lab, \
                                        'dilution_lab':dilution_lab, \
                                        'method_lab':method_lab, \
                                        'value_lab':value_lab, \
                                        'description_lab':description_lab, \
                                        'person_abbrev_lab':person_abbrev_lab, \
                                        'timestamp_start_lab':timestamp_start_lab, \
                                        'timestamp_end_lab':timestamp_end_lab, \
                                        'site':site, \
                                        'person_abbrev_sample':person_abbrev_sample, \
                                        'method_sample':method_sample, \
                                        'filter_sample':filter_sample, \
                                        'dilution_sample':dilution_sample, \
                                        'timestamp_sample':timestamp_sample, \
                                        'description_sample':description_sample}, ignore_index=True)
            #print(description_lab)
                                       
        
        whdpinst.logging(log,'** Variable: '+varname+' - Number of entries: '+str(nValue)+' done')
        
    whdpinst.logging(log,'* Data transformation done')

    # ---------------------
    
    whdpinst.logging(log,'* Completeness check ..')
    nRow0  = df_out.shape[0]
    if nRow0>=0:
        tf     = (~(pd.isnull(df_out[required]))).all(axis=1)
        df_out = df_out.loc[tf==True,:]
        nRow   = df_out.shape[0]
        whdpinst.logging(log,'* Completeness check done - Retained: '+str(nRow)+' of '+str(nRow0))

    ########################################################################
    ### OUTPUT ### 
    ########################################################################
    from unidecode import unidecode
    df_out['description_sample']=df_out['description_sample'].astype(str).apply( lambda x:  unidecode(x))
    df_out['description_lab']=df_out['description_lab'].astype(str).apply( lambda x:  unidecode(x))
    
    # Store dataframe as file
    filetarget     = os.path.join(whdpinst.path_lab_operational,'lab_results.csv')
    filetarget_txt = os.path.join(whdpinst.path_lab_operational,'lab_results.csv.txt')

    # --- remove old ---
    whdpinst.removefile(log,filetarget_txt)
    whdpinst.removefile(log,filetarget)
    
    # --- write new ---
    whdpinst.logging(log,'* Writing: '+filetarget_txt+' ..')
    df_out.to_csv(path_or_buf=filetarget_txt, sep=';', na_rep='',index =False,float_format='%.6f', encoding = 'ascii')
    whdpinst.logging(log,'* Writing: '+filetarget_txt+' done')

    # --- rename new ---
    whdpinst.logging(log,'* Renaming: '+filetarget_txt+' to '+filetarget+' ..')
    os.rename(filetarget_txt, filetarget)
    whdpinst.logging(log,'* Renaming: '+filetarget_txt+' to '+filetarget+' done')

###############
###  EMAIL  ###
###############

subject = '[WHDPauto] Modified lab results.'
body = 'Checked for lab results.'
whdpinst.sendmail(log,subject,body)

############
### DONE ###
############

whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
whdpinst.logging(log,'* DONE ')
