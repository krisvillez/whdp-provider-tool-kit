#!/usr/bin/python

root  = r'\\\\eaw-depts\\eng$'
folder    = [r"\\IngData\\AOS\\Allgemein\\Archiv_Labornummern",\
             r"\\IngData\\AOS\\Allgemein"]
filenames = ["LabSamples2018.xlsx",\
             "LabSamples2019.xlsx"]

###################
### ENVIRONMENT ###
###################

import datetime
import os
import shutil
import sys

sys.path.append('../..')

from whdpprovider.whdpkit import whdpkit

whdpinst = whdpkit()

if whdpinst.AtWHDP:
    pass
else:
    from pathlib import Path

###############
###  START  ###
###############
log = whdpinst.log_t_lab
whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

##################################
###  GET FILES ###
##################################

for v, file in enumerate(filenames):
    
    whdpinst.logging(log,'* Getting: '+file+' ..')
    
    file_target = os.path.join(whdpinst.path_lab_local,file)
    
    login   = whdpinst.connect_lab['login']
    pw      = whdpinst.connect_lab['pw']
    
    command = "smbclient -U '" + login + "%" + pw + "' "+root+folder[v]+" -c 'get "+file+" "+file_target+"'"
    print(command)
    
    if whdpinst.AtWHDP:
        os.system(command)
    else:
        file_source = os.path.join('\\'+str(Path(root+folder[v])),file)
        shutil.copy2(file_source, file_target)
    
    whdpinst.logging(log,'* Getting: '+file+' done')

###############
###  EMAIL  ###
###############

subject = '[WHDPauto] Copied lab files succesfully from '+root+'.'
body = 'Lab files were copied succesfully from SMB server.'
whdpinst.sendmail(log,subject,body)

############
### DONE ###
############

whdpinst.logging(log,'* current time [yyyy-mm-dd HH:MM:SS]: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
whdpinst.logging(log,'* DONE ')